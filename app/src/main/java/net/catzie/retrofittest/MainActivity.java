package net.catzie.retrofittest;

import android.app.ListActivity;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private String repo_owner = "octocat";
    private String repo_name = "Hello-World";

    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "start of onCreate()");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tvRepoOwner = (TextView) findViewById(R.id.repo_owner);
        TextView tvRepoName = (TextView) findViewById(R.id.repo_name);
        tvRepoOwner.setText(repo_owner);
        tvRepoName.setText(repo_name);


        /**
         * RETROFIT
         */

        // Create a very simple REST adapter which points the GitHub API endpoint.
        GitHubClient client = ServiceGenerator.createService(GitHubClient.class);

        // Fetch and print a list of the contributors to this library.
        Call<List<Contributor>> call =
                client.contributors(repo_owner, repo_name);

        try {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            // May cause NetworkOnMainThreadException if not called in thread or async class
            List<Contributor> contributors = call.execute().body();


            List<String> stringArray = new  ArrayList<String>();
            if(contributors != null) {
                for (Contributor contributor : contributors) {
                    Log.d( TAG, "contributors: " + contributor.login + " (" + contributor.contributions + ")");
                    stringArray.add(contributor.login + " (" + contributor.contributions + ")");
                }

                ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.activity_listview, stringArray);
                ListView listView = (ListView) findViewById(R.id.contributorsList);
                listView.setAdapter(adapter);


            } else {
                Log.d( TAG, "contributors: null");
            }

        } catch (IOException e) {
            // handle errors
            e.printStackTrace();
            Log.d(TAG, e.getMessage());
        }


        Log.d(TAG, "end of onCreate()");
    }

}
